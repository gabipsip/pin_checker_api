import sqlite3
import random
import json
from flask import Flask
from flask_hashing import Hashing

app = Flask(__name__)
hashing = Hashing(app)


def get_db_connection():
    conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    return conn


def get_random_pin():
    list_figures = [(random.randrange(0, 9)) for i in range(6)]
    string_figures = ''.join([str(elem) for elem in list_figures])
    return string_figures


@app.route('/pin/', methods=["GET"])
def route_pin():
    conn = get_db_connection()
    cur = conn.cursor()
    new_pin = get_random_pin()
    h = hashing.hash_value(new_pin, salt="abcd")
    cur.execute("INSERT INTO pins (pinCode) VALUES (?)", (h,))
    conn.commit()
    conn.close()
    return new_pin


@app.route('/login/', methods=["GET", "POST"])
def route_login():
    with open('pin.json') as f:
        data = json.load(f)
        pin_to_check = data.get("pinCode")
        conn = get_db_connection()
        cur = conn.cursor()
        h = hashing.hash_value(pin_to_check, salt="abcd")
        cur.execute("SELECT * FROM pins WHERE pinCode=? AND pinUsed= 0", (h,))
        result = cur.fetchone()
        if result:
            cur.execute("""UPDATE pins SET pinUsed = 1 WHERE pinCode= ?""", (h,))
            conn.commit()
            conn.close()
            return "ok"
        else:
            return "ko"
