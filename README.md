PIN CHECKER API
===============

Flask : 1.1.2
python3 :  3.8.5
sqlite2


**To launch the app run the following command :** 
+ source env/bin/activate
+ export FLASK_APP=app
+ export FLASK_ENV=development
+ flask run

http://127.0.0.1:5000/pin/ --> Display a random pin hash it and stock into database
http://127.0.0.1:5000/login/ --> Get pinCode from json file check if it match with a pincode from database, check if it's valid, then display OK or KO.





